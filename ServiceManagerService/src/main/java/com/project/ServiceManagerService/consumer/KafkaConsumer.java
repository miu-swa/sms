package com.project.ServiceManagerService.consumer;

import com.project.ServiceManagerService.entity.CSSEvent;
import com.project.ServiceManagerService.service.ServiceManagerService;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaConsumer {
    @Autowired
    ServiceManagerService managerService;

    @KafkaListener(topics = "newRTD", groupId = "group-1")
    public void consume(ConsumerRecord<?,?> message) {
        System.out.println(message.partition()+","+message.topic()+","+message.value());
    }
    @KafkaListener(topics = "newRS", groupId = "group-1")
    public void consume1(CSSEvent event) {
        managerService.receive(event);
    }
}
