package com.project.ServiceManagerService.controller;

import com.project.ServiceManagerService.entity.CSSEvent;
import com.project.ServiceManagerService.service.ServiceManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.ExecutionException;

@RestController
public class ServiceManagerController {
    @Autowired
    ServiceManagerService managerService;

    @PostMapping("/getAllServiceInfo")
    public ResponseEntity<?> getAllServiceInfo() throws ExecutionException, InterruptedException {
        return new ResponseEntity<>(managerService.getAllServiceInfo(), HttpStatusCode.valueOf(200));
    }
}
