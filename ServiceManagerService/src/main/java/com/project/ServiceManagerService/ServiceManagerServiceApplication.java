package com.project.ServiceManagerService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceManagerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceManagerServiceApplication.class, args);
	}

}
