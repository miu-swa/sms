package com.project.ServiceManagerService.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.ServiceManagerService.entity.CSSEvent;
import com.project.ServiceManagerService.producer.TopicProducer;
import com.project.ServiceManagerService.service.ServiceManagerService;
import com.project.ServiceManagerService.wrapper.InputMessageWrapper;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.ListTopicsOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import java.io.DataInput;
import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ExecutionException;

@Service
public class ServiceManagerServiceImpl implements ServiceManagerService {
    @Override
    public Set<String> getAllServiceInfo() throws ExecutionException, InterruptedException {
        Properties properties = new Properties();
        properties.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");

        AdminClient adminClient = AdminClient.create(properties);

        ListTopicsOptions listTopicsOptions = new ListTopicsOptions();
        listTopicsOptions.listInternal(true);

        return adminClient.listTopics(listTopicsOptions).names().get();
    }

    @Override
    public void receive(CSSEvent event) {
        try {

            ObjectMapper mapper = new ObjectMapper();
            System.out.println("got a message from client: " + event);

            //converting the message to an object
            InputMessageWrapper inputMessageWrapper = mapper.readValue((DataInput) event, InputMessageWrapper.class);

            String serviceName = inputMessageWrapper.getServiceName();
            Set<String> topics = inputMessageWrapper.getTopics();
            String interval = inputMessageWrapper.getInterval();

//            System.out.println("\nReceived message= File name: " + Arrays.toString(event.getZipFile()) + ", Service name: "
//                    + event.getServiceName() + ", Message: " + event.getTopics());

            byte[] zipFile = event.getZipFile();
            FileCopyUtils.copy(zipFile, new File("documentJars/"+serviceName+".zip"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
