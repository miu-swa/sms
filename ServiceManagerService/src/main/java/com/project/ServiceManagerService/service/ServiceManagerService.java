package com.project.ServiceManagerService.service;

import com.project.ServiceManagerService.entity.CSSEvent;

import java.util.Set;
import java.util.concurrent.ExecutionException;

public interface ServiceManagerService {
    Set<String> getAllServiceInfo() throws ExecutionException, InterruptedException;
    void receive(CSSEvent event);
}
