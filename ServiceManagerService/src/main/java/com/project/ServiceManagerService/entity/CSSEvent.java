package com.project.ServiceManagerService.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CSSEvent {
    private byte[] zipFile;
    private String serviceName;
    private Set<String> topics;
}
