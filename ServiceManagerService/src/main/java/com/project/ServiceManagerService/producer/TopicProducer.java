package com.project.ServiceManagerService.producer;


import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Service
public class TopicProducer {
    KafkaProducer <String, String> producer;
    public void send(@Payload String message, String topic){
            producer.send(new ProducerRecord<>(topic, message));
    }

}
